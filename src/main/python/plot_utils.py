import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os.path
import numpy as np
#
# Assuming each series is a dict with: 'values', 'min','max','label','log_scale'
#
def plot_data(seriesx, seriesyleft, seriesyright, filename_fig):
    if len(seriesx['values']) == 0 or len(seriesyright['values']) == 0 or len(seriesyleft['values'])==0:
        print("Error: no data, no plot!")
        return
    #See http://www.randalolson.com/2014/06/28/how-to-make-beautiful-data-visualizations-in-python-with-matplotlib/

    # You typically want your plot to be ~1.33x wider than tall. This plot is a rare
    # exception because of the number of lines being plotted on it.
    # Common sizes: (10, 7.5) and (12, 9)
    myfontsize = 13
    plt.figure(figsize=(5.1, 4),dpi=100)

    # Remove the plot frame lines. They are unnecessary chartjunk.
    fig, ax1 = plt.subplots()

    # Ensure that the axis ticks only show up on the bottom and left of the plot.
    # Ticks on the right and top of the plot are generally unnecessary chartjunk.
    ax1.get_xaxis().tick_bottom()
    ax1.get_yaxis().tick_left()
    ax1.spines["top"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    ax1.spines["bottom"].set_visible(True)
    ax1.spines["bottom"].set_color('#aaaaaa')
    ax1.spines["bottom"].set_linestyle('dashed')
    ax1.spines["left"].set_visible(True)
    ax1.spines["left"].set_color('#aaaaaa')
    ax1.spines["left"].set_linestyle('dashed')
    if 'log_scale' in seriesyleft:
        ax1.set_yscale('log')
    ax1.set_xlabel(seriesx['label_axis'],fontsize=myfontsize+1)
    ax1.set_ylabel(seriesyleft['label_axis'],fontsize=myfontsize+1)
    if 'log_scale' in seriesx:
        ax1.set_xscale('log')

    ax2 = ax1.twinx()
    ax2.set_ylabel(seriesyright['label_axis'],fontsize=myfontsize+1)
    ax2.spines["top"].set_visible(False)
    ax2.spines["right"].set_visible(True)
    ax2.spines["right"].set_color('#aaaaaa')
    ax2.spines["right"].set_linestyle('dashed')
    ax2.spines["bottom"].set_visible(False)
    ax2.spines["left"].set_visible(False)
    if 'log_scale' in seriesyright:
        ax2.set_yscale('log')


    # Limit the range of the plot to only where the data is.
    # Avoid unnecessary whitespace.
    xmin = seriesx['values'][0]
    if 'min' in seriesx:
        xmin = seriesx['min'] #+ _some_margin_axis(seriesx['values'],True)
    xmax = seriesx['values'][-1]
    if 'max' in seriesx:
        xmax = seriesx['max'] #+ _some_margin_axis(seriesx['values'],False)
    plt.xlim(xmin,xmax)
    print("plot limits %s x axis: [%.5f,%5.f]" % (seriesx['values'],xmin,xmax))

    # Make sure your axis ticks are large enough to be easily read.
    # You don't want your viewers squinting to read your plot.
    #plt.yticks(runtime_values[0:2:-1], ["%d" for x in runtime_values[0:2:-1]], fontsize=11)
    #plt.yticks(fontsize=myfontsize)
    #plt.xticks(fontsize=myfontsize)

    #See http://stackoverflow.com/questions/14530113/set-ticks-with-logarithmic-scale
    #step_x = max(len(seriesx['values'])/10,1)
    if 'ticks' in seriesx:
       if 'tick_labels' in seriesx:
           ax1.set_xticks(seriesx['ticks'])
           ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
           ticks=ax1.get_xticks().tolist()
           for i in range(0,len(ticks)):
               ticks[i] = seriesx['tick_labels'][i]
           ax1.set_xticklabels(ticks)
       else:
           ax1.set_xticks(seriesx['ticks'])
           ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())

    if 'ticks' in seriesyleft:
        ax1.set_yticks(seriesyleft['ticks'])
        ax1.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())

    if 'ticks' in seriesyright:
        ax2.set_yticks(seriesyright['ticks'])
        ax2.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    #step_y_left = max(len(seriesyleft['values'])/10,1)
    #ax1.set_yticks(seriesyleft['values'][0:-1:step_y_left])
    #ax1.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())

    #step_y_right = max(len(seriesyright['values'])/10,1)
    #ax2.set_yticks(seriesyright['values'][0:-1:step_y_right])
    #ax2.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())

    # Provide tick lines across the plot to help your viewers trace along
    # the axis ticks. Make sure that the lines are light and small so they
    # don't obscure the primary data lines.
    #for y in log_runtime_values[0:2:-1]:
    #    plt.plot(log_runtime_values[0:2:-1], [y] * len(log_runtime_values[0:2:-1]), "--", lw=0.5, color="black", alpha=0.3)

    # Remove the tick marks; they are unnecessary with the tick lines we just plotted.
    ax1.tick_params(axis="both", which="both", bottom="off", top="off",
                    labelbottom="on", left="off", right="off", labelleft="on", labelsize=myfontsize-2)
    ax2.tick_params(axis="both", which="both", bottom="off", top="off",
                    labelbottom="off", left="off", right="off", labelright="on", labelsize=myfontsize-2)


    # Plot each line separately with its own color, using the Tableau 20
    # color set in order
    line_left, = ax1.plot(seriesx['values'],
            seriesyleft['values'],
            "-",
            lw=1.0, color=(0.2,0.5,0.7), marker='x',
            label= seriesyleft['label'])
    ymin_left = seriesyleft['values'][0] + _some_margin_axis(seriesyleft['values'],True)
    if 'min' in seriesyleft:
        ymin_left = seriesyleft['min']
    ymax_left = seriesyleft['values'][-1]  + _some_margin_axis(seriesyleft['values'],True)
    if 'max' in seriesyleft:
        ymax_left = seriesyleft['max']
    ax1.set_ylim(ymin=ymin_left, ymax=ymax_left)

    line_right, = ax2.plot(seriesx['values'],
            seriesyright['values'],
            '--',
            lw=1.0, color=(1.0,0.5,0.1), marker='o',
            label=seriesyright['label']
            )
    ymin_right = seriesyright['values'][0]  + _some_margin_axis(seriesyright['values'],True)
    if 'min' in seriesyright:
        ymin_right = seriesyright['min']
    ymax_right = seriesyright['values'][-1] + _some_margin_axis(seriesyright['values'],False)
    if 'max' in seriesyright:
        ymax_right = seriesyright['max']
    ax2.set_ylim(ymin=ymin_right,ymax=ymax_right)

    plt.legend((line_left, line_right),(seriesyleft['label'], seriesyright['label']), loc='lower right', prop={'size':10})

    fig.set_size_inches(5.1,4)
    plt.gcf().subplots_adjust(bottom=0.05)
    plt.tight_layout()
    fig.savefig(filename_fig, dpi=100)
    print("Saved %s" % filename_fig)


def _some_margin_axis(list, left):
    margin = np.std(list) * 2
    if left:
        if list[0] < list[-1]: #ascending
            return - margin
        else:
            return  margin
    else:
        print("%s -> %.3f" % (list,margin))
        if list[0] < list[-1]:  #ascending
            return margin
        else:
            return -margin
