#See also http://pbpython.com/simple-graphing-pandas.html
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os.path
import sys

# ============= PARAMETERS ================ 
input_dir = '../../../temp/'
files = []
labels = ['Our', 'WINEPI','LAXMAN','MARBLES']
#moby dick
dataset = 'moby'
if len(sys.argv) == 2:
    dataset = sys.argv[1]
    print('dataset is %s' % dataset)

if dataset == 'moby':
    dir = input_dir 
    files.append(dir + "mine_patterns_moby_dick_gutenberg_processed_minSup=004_minCohesion=0.0100_modus=MEAN_maxLen=005.csv")
    files.append(dir + "closepi_moby_dick_gutenberg_processed_minSup=30.000_window=015_modus=regular.csv")
    files.append(dir + "closepi_moby_dick_gutenberg_processed_minSup=4.000_window=015_modus=min_windows.csv")
    files.append(dir + "closepi_moby_dick_gutenberg_processed_minSup=1.000_window=015_modus=weighted.csv")
if dataset == 'origin':
    dir = input_dir 
    files.append(dir + "mine_patterns_origin_of_species_processed_minSup=005_minCohesion=0.0100_modus=MEAN_maxLen=005.csv")
    files.append(dir + "closepi_origin_of_species_processed_minSup=30.000_window=015_modus=regular.csv")
    files.append(dir + "closepi_origin_of_species_processed_minSup=5.000_window=015_modus=min_windows.csv")
    files.append(dir + "closepi_origin_of_species_processed_minSup=1.000_window=015_modus=weighted.csv")

for file in files:
    if not os.path.isfile(file):
       print('File not found %s' % file)
       exit(1)
# ===========================================

def basic_parse(input):
    data = None
    if "mine_patterns" in input:
        data=pd.read_csv(input,sep=';',names=['pattern','frequency','len','cohesion','empty'])
        data=data.sort_values(['cohesion','len','frequency','pattern'], ascending=[False,False,False,True])
    else:
        data=pd.read_csv(input,sep=';',names=['pattern','frequency','m-windows', 'len','empty'])
        if "regular" in input:
            data=data.sort_values(['frequency','len','pattern'], ascending=[False,False,True])
        else:
            data=data.sort_values(['m-windows','len','pattern'], ascending=[False,False,True])
    return data;


def parse_file(input, top):
    data = basic_parse(input)
    data2 = data[['frequency','len']][:top]
    #print(data2.head())
    len_group = data2.groupby('len').size()
    #print(len_group)
    return [len_group.keys().values, len_group.values]


for i in range(0,len(files)):
    name = os.path.basename(files[i])
    tokens = name.split("_")
    print("input for %s\nfile: %s" % (labels[i], name))
    for token in tokens[-4:-1]:
        print("param:%s" % token)
    #print top 100
    data = basic_parse(files[i])
    print("patterns:#%d" % (data.shape[0]))
    if "mine_patterns" in files[i]:
         data = data[['cohesion','len','frequency','pattern']]
    elif "regular" in files[i]:
         data = data[['frequency','len','pattern']]
    else:
         data = data[['m-windows','len','pattern']]
    print("=========== TOP 15 ===============")
    print(data.head(15))
    print("==================================\n\n\n")



#For each algorithm (dataset), show number of patterns for each size
for top in [100,500,2500]:
    datasets = [parse_file(file,top) for file in files]
    #print labels
    print("Lengths for patterns (top %d):" % top)
    for i in range(len(files)):
        sys.stdout.write("\t%12s" % labels[i])
    print("")
    #print 'matrix'
    for i in range(2,15): #for each pattern length, show count for each dataset
        sys.stdout.write("|p| = %5d" % i)
        for dataset in datasets:
            count = '/'
            if i in dataset[0]:
                index = np.nonzero(dataset[0] == i)[0]
                count = dataset[1][index] #e.g. get count 1000 for 2, given [[2,3],[1000,100]]
            sys.stdout.write("   %12s" % count[0])
        print("")



#For each algoritme: overlap met onze methode in percentages
def parse_file2(input, top):
    data = basic_parse(input)
    data2 = data[['pattern']][:top]
    return data2.values.tolist()

#e.g. [mobi, dick] same as [dick,mobi]
def same_pattern(pattern1, pattern2):
    if len(pattern1) != len(pattern2):
        return False
    if not pattern1[0] in pattern2:
        return False
    for token1 in pattern1:
        if not (token1 in pattern2):
           return False
    return True

for top in [100,500,2500]:
    datasets = [parse_file2(file,top) for file in files]
    for i in range(1,len(files)):
        our_method = datasets[0]
        other_method = datasets[i]
        table1 = [token[0].split() for token in our_method]
        table2 = [token[0].split() for token in other_method]
        #compare all keys (using classic quadratic method):
        overlap = []
        for pattern1 in table1:
            for pattern2 in table2:
                #print('%s == %s? %s', pattern1, pattern2, same_pattern(pattern1,pattern2))
                if same_pattern(pattern1,pattern2):
                    overlap.append(pattern1)
                    #could break here
        print('Set %s and set %s overlap in %d (top %d)' % (labels[0], labels[i], len(overlap), top))
        #for p in overlap[:5]:
        #    print(">>%s" % p)
        #if len(overlap) > 5:
        #    print("...")
    #end for

#compare ranking...
def parse_file3(input, top):
    data = basic_parse(input)[:top]
    #print(data.head())
    return data.values.tolist()

datasets = [parse_file3(file, -1) for file in files]

topK = 10
import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "."))
from ordered_set import OrderedSet
patterns = OrderedSet()
for dataset in datasets:
    for row in dataset[:topK]:
        pattern = row[0]
        found = False
        for pattern_added in patterns:
            if same_pattern(pattern.split(), pattern_added.split()):
                found = True
        if not found:
            patterns.add(pattern)

rankings = []
print("%-24s   %-10s    %-10s    %-10s    %-10s" % ("Pattern",labels[0],labels[1],labels[2],labels[3]))
for pattern in patterns:
    ranking = []
    for i in range(0,len(datasets)):
        dataset = datasets[i]
        rank_on_dataset = -1
        for row_idx in range(0,len(dataset)):
            if same_pattern(pattern.split(),dataset[row_idx][0].split()):
                rank_on_dataset = row_idx + 1
                break
        ranking.append(rank_on_dataset)
    print("%-24s   %-10s   %-10s   %-10s   %-10s" % (pattern,ranking[0],ranking[1],ranking[2],ranking[3]))
    rankings.append(ranking)

print("%-24s   %-10s    %-10s    %-10s    %-10s" % ("Total//",len(datasets[0]),len(datasets[1]),len(datasets[2]),len(datasets[3])))
