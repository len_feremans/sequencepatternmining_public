package be.uantwerpen.pattern_mining;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import be.uantwerpen.util.CountMap;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.LRUCache;
import be.uantwerpen.util.Pair;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

/**
 * Created: 6-Jan-2016
 * @author lfereman
 *
 * Find patterns in sequential (discretized) data
 * 
 * Based on Efficient Discovery of Sets of Co-occuring Items in Event Sequences by Boris Cule, and Len Feremans
 * 
 * Patterns are found based using the following contraints:
 * - Minimal frequency (or support), e.g. pattern "abc" if a,b and c occur > min_freq
 * - Minimal cohesion, e.g. pattern "abc" if cohesion > min_cohesion,
 * 	 where cohesion is computed as the average width of minimal window
 *   in which a,b and c occur, proportional to alphabet size. 
 *   For example if "aebec" then minimal window is 5, and cohesion is 3/5
 * - Maximum size of pattern
 * 
 * Implementation
 * - DONE: Efficient int[] based reverse-index
 * - DONE: Test on 'large' data, in casu, moby dick, but also 20newsgroups
 * - DONE: Window computation -> beter
 * - DONE: Overal, custom  datastructures, less memory consumption -> int[] instead of 'fancy' stuff
 * - DONE: DFS: memory waisting fixed (y never copied)
 * - DONE: MinimalWindow Lazy computation
 * - DONE: Make input 'streaming' (e.g. nextToken() returns next synmbol file)
 * 
 * NOT DONE:
 * - DO?: Multi-threaded 
 * 
 */
public class MineCoOccuringItems {

	public static boolean DEBUG_LOAD_DATA = true;
	public static boolean DEBUG_DFS = true;
	public static boolean DEBUG_DICT = false;
	public static boolean DEBUG_WINDOW_COMPUTATION = false;
	public static boolean ENABLE_LAZY_WINDOWS = true;
	public static boolean LRU_CACHE = false;

	private File inputLabels;
	private File inputSequence;
	private File output;

	private int minSupport;
	
	private IndexStructure inverseIndex; //Cache
	public int[] frequentItems; //contains frequency... internally using auto-increment ID's for each item
	private String[] frequentItemsLabels; //from item(17) to  "hello"
	private Map<String,Integer> tokenToItemIdx; //from "the" to item(0)
	
	private List<int[]> windows_final = new ArrayList<int[]>(); //only if DEBUG_WINDOW_COMPUTATION is true
	
	/**
	 * frequency must >= then minSupport 
	 * 
	 * @param data
	 * @param minSupport
	 * @param maxLength
	 * @param minCohesion
	 */
	public MineCoOccuringItems(File inputLabels, File inputSequence, int minSupport) throws IOException{
		this.inputLabels = inputLabels;
		this.inputSequence = inputSequence;
		this.minSupport = minSupport;
		loadData();
	}
	
	/**
	 * @param data
	 * @throws IOException 
	 */
	private void loadData() throws IOException {
		//Fase1: first 'stream' over file, to get frequencies 
		CountMap<Long> count_map = new CountMap<Long>(); //abstraction over TreeMap
		FileStream fs = new FileStream(inputSequence,' ','\n');
		String token = fs.nextToken();
		int size = 0;
		while(token !=null){
			count_map.add(Long.parseLong(token));
			size++;
			token = fs.nextToken();
		}
		Map<Long,Integer> count_map_pruned = new HashMap<Long,Integer>();
		for(Entry<Long,Integer> entry: count_map.getMap().entrySet()){
			if(entry.getValue() >= this.minSupport){
				count_map_pruned.put(entry.getKey(), entry.getValue());
			}
		}
		int originalSize = count_map.getMap().size();
		count_map.getMap().clear();
		count_map = null;
		//Load labels
		FileStream fs2 = new FileStream(inputLabels,' ','\n');
		List<String> labels = new ArrayList<String>();
		String label = fs2.nextToken();
		while(label != null){
			labels.add(label);
			label = fs2.nextToken();
		}
		//Fase2: Build basic datastructure: 
		// - Items represented by unique id
		// - sorted on frequency
		// - map to translate from token in raw data to item id, and vice-versa
		int frequentItemSize = count_map_pruned.entrySet().size();
		List<Pair<Long,Integer>> listOfFrequentTokens = new ArrayList<Pair<Long,Integer>>(frequentItemSize);
		for(Entry<Long,Integer> entry: count_map_pruned.entrySet()){
			listOfFrequentTokens.add(new Pair<Long,Integer>(entry.getKey(), entry.getValue()));
		}
		Collections.sort(listOfFrequentTokens, new Comparator<Pair<Long,Integer>>() {

			@Override
			public int compare(Pair<Long, Integer> o1, Pair<Long, Integer> o2) {
				return o1.getSecond() - o2.getSecond();
			}
		});
		this.frequentItems = new int[frequentItemSize];
		this.frequentItemsLabels = new String[frequentItemSize];
		this.tokenToItemIdx = new HashMap<String,Integer>();
		for(int i=0; i< frequentItemSize; i++){
			this.frequentItems[i] = listOfFrequentTokens.get(i).getSecond();
			long id = listOfFrequentTokens.get(i).getFirst();
			this.frequentItemsLabels[i] = labels.get((int)id);
			this.tokenToItemIdx.put(frequentItemsLabels[i], i);
		}
		//Fase4: Loop again over file, this time make inverse index...
		//note: Using custom datastructure here, to conserve memory, and have fast 'array' index
		//much faster
		this.inverseIndex = new IndexStructure(this.frequentItems);
		fs = new FileStream(inputSequence,' ','\n');
		int position = 0;
		token = fs.nextToken();
		while(token != null){
			Integer tokenIdx = tokenToItemIdx.get(labels.get(Integer.valueOf(token)));
			if(tokenIdx != null) {
				this.inverseIndex.push(tokenIdx, position);
			}
			else{//pruned... do nothing	
			}
			token = fs.nextToken();
			position++;
		}
		//debug
		debugLoadData(size, originalSize);
	}

	public void minePatterns(int maxLength, double minCohesion) throws IOException{
		//4 do depth first search
		//Note: as y decreases every time DFS goes deeper, just keep depth. Y is then sub-array starting at depth
		minePatterns(maxLength, minCohesion, new int[0]);
	}
	
	public void minePatterns(int maxLength, double minCohesion, int[] pattern) throws IOException {
		output = new File(String.format("./temp/mine_patterns_%s_minSup=%03d_minCohesion=%03.4f_modus=MEAN_maxLen=%03d.csv", 
				Utils.getFilenameNoExtension(this.inputLabels), this.minSupport, minCohesion,  maxLength));
		if(!output.getParentFile().exists())
			output.getParentFile().mkdirs();
		BufferedWriter writer = new BufferedWriter(new FileWriter(output));
		Timer timer = new Timer(String.format("minePatterns(minSup=%d,minCohesion=%.4f,modus=%s,maxLen=%s)", this.minSupport, minCohesion, "mean", maxLength));
		//Do depth first search
		//Note: as y decreases every time DFS goes deeper, just keep depth. Y is then sub-array starting at depth
		ArrayDeque<Pair<int[],Integer>> stack = new ArrayDeque<Pair<int[], Integer>>(); 
		stack.add(new Pair<int[],Integer>(pattern, 0));
		long noPatterns = 0;
		long visits = 0;
		long startTime = System.currentTimeMillis();
		long lastPrint = startTime;
		while(!stack.isEmpty()){
			//progress reporting:
			visits++;
			if(visits % 1000 == 0){
				long elapsed =  System.currentTimeMillis() - lastPrint;
				if(elapsed > 1000 * 10){ //every 10 seconds
					 printDebug(stack, noPatterns, visits, startTime,elapsed);
					 lastPrint = System.currentTimeMillis();
				}
			}
			//1. pop element 
			Pair<int[],Integer> pair = stack.removeFirst();
			int[] x = pair.getFirst();
			int depth = pair.getSecond();
			double cohesionPruningScore = maxCohesionForPruning(x,depth, maxLength, minCohesion); //PERFORMANCE OK?
			//2. prune 
			if(cohesionPruningScore  < minCohesion){
				continue;
			}
			//3. check stop
			if(depth == this.frequentItems.length)
			{
				if(x.length > 1){
					noPatterns++;
					//save to file
					double freqX= frequency(x);
					String patternReabable = toReadablePattern(x);
					String str = String.format("%s;%d;%d;%.6f;\n", patternReabable, (int)freqX, x.length, cohesionPruningScore);
					writer.write(str);
					continue;
				}
				else
					continue;
			}
			//4 divide-and-conquor step
			int a = depth; //might seem strange, but y[0] = 0, and y[1] =1...
			//4.1 add element (and visit children)
			if(x.length + 1 <= maxLength){
				//visit X U {a}, Y \ {a}
				int x_new[] = new int[x.length+1];
				System.arraycopy(x, 0, x_new, 0, x.length);
				x_new[x.length] = a;
				stack.addFirst(new Pair<int[],Integer>(x_new,depth+1));
			}
			//4.2 go further without element
			//visit X, Y \ {a}
			stack.addFirst(new Pair<int[],Integer>(x,depth+1)); //x is immutable, so no copy is necessary!
		}
		timer.end();
		writer.close();
		System.out.println("No visits: " + visits);
		System.out.println("No patterns: " + noPatterns);
		System.out.println("Saved " + this.output.getName());
	}

	/**
	 * Compute cohesion upper bound:
	 * 
	 * C(x,y) = lenXY * (freqX + freqY) / (minWindows + lenXY * freqY)
	 * where lenXY = min(max_size, length(x + y))
	 * 		 b = frequency(x)
	 * 		 freqY = N_max(x,y) which is N(y) if length(x + y) <= max_size 
	 * 							  or (assuming Y elements are sorted on decreasing frequency)
	 * 								 N(substring(y,0,max_size - |X|)
	 * 		 minWindows = sum_minimal_windows(X)
	 * @param x
	 * @param y
	 * @return
	 */
	public double maxCohesionForPruning(int[] x, int depth, int maxLength, double minCohesion) {
		int ylen = this.frequentItems.length - depth;
		double lenXY = Math.min(maxLength, x.length + ylen);
		double freqX = frequency(x);
		double freqY = 0.0;
		//note: could also be pre-computed...
		for(int i=depth; i-depth<maxLength - x.length && i <this.frequentItems.length; i++){
			int ch = depth; //same as y[i]
			freqY += this.frequentItems[ch];
		}
		//VALUATE SUM LAZY
		double maxMinWindows = Integer.MAX_VALUE;
		if(ENABLE_LAZY_WINDOWS){
			maxMinWindows = lenXY *  (freqX + freqY) / minCohesion - lenXY * freqY;
		}
		double minWindows = sumMinimalWindows(x, maxMinWindows); 
		if(minWindows == Double.MAX_VALUE)
			return Double.MIN_VALUE;
		//cohesion:
		double maxCohesion = lenXY * (freqX + freqY) / (minWindows + lenXY * freqY);
		return maxCohesion;
	}
	
	public double frequency(int[] pattern){
		double freq = 0.0;
		for(int xi: pattern){
			freq += this.frequentItems[xi];
		}
		return freq;
	}
	
	public double cohesion(int[] pattern){
		double freqX = frequency(pattern);
		double sumW = sumMinimalWindows(pattern);
		double cohesion = pattern.length * freqX /sumW;
		return cohesion;
	}
	
	public double sumMinimalWindows(int[] x){
		return sumMinimalWindows(x,Integer.MAX_VALUE);
	}
	
	/**
	 * Loop over stream, from left to right...
	 * 'gather' most left window (== minimal window)
	 *
	 * If maxMinWindows>running sum -> return  maxMinWindows
	 * 
	 * Note: Expects Integers to be frequent
	 * @param x
	 * @return
	 */
	
	private LRUCache<String, Double> cache = null;
	
	private double sumMinimalWindows(int[] x, double maxMinWindows){
		if(x.length == 0){
			return 1;
		}
		if(x.length == 1){
			return this.frequentItems[x[0]]; 
		}
		//cache
		String key = null;
		if(LRU_CACHE){
			if(cache == null) 
				cache = new LRUCache<String,Double>(10000);
			key = toReadablePattern(x);
			if(cache.get(key) != null){
				return cache.get(key);
			}
		}
		//note: note multi-threaded possible...
		windows_final.clear();
		int position[][] = new int[x.length][]; //holds positions for each element, e.g. 'a': 1 3 10, 'b': 2, 4, 5
		int lastPosition[] = new int[x.length]; //holds lastPosition, e.g. 'a': 4, 'b':2 
		int startFrom[] = new int[x.length];	//holds next (not-visited) offset in positions e.g. 'a': 1, 'b': 1 (after lastPosition)
		List<int[]> windows_current = new ArrayList<int[]>();
		int frequency = 0;
		for(int i=0; i<x.length; i++){
			int ch = x[i];
			int positions_array[] = this.inverseIndex.get(ch);
			frequency += positions_array.length;
			position[i] = positions_array;
			startFrom[i] = 0;
			lastPosition[i] = Integer.MIN_VALUE;
		}
		int prev_min = Integer.MIN_VALUE;
		double running_sum = 0.0;
		for(int idx=0; idx<frequency;idx++){
			//get current position
			int pos = Integer.MAX_VALUE;
			for(int i=0; i<x.length; i++){
				if(startFrom[i] != position[i].length){
					pos = Math.min(pos, position[i][startFrom[i]]);
				}
			}
			//update lastPosition
			for(int i=0; i<x.length; i++){
				if(startFrom[i] != position[i].length && position[i][startFrom[i]] == pos){
					lastPosition[i] = pos;
					startFrom[i] = startFrom[i]+1;
				}
			}
			//compute window_size
			int min_pos = Integer.MAX_VALUE;
			int max_pos = Integer.MIN_VALUE;
			for(int lastPos: lastPosition){
				min_pos = Math.min(lastPos, min_pos);
				max_pos = Math.max(lastPos, max_pos);
			}
			int w = Integer.MAX_VALUE;
			if(min_pos != Integer.MIN_VALUE){ //at beginning, lastPos might be Integer.MIN_VALUE
				w = max_pos - min_pos + 1;
			}
			//check condition:
			boolean condition = min_pos != Integer.MIN_VALUE && prev_min < min_pos; 
			if(condition){
				//update windows with minimal size
				List<int[]> newWindows = new ArrayList<int[]>(windows_current.size());
				for(int window[]: windows_current){
					window[1] = Math.min(window[1], max_pos - Math.min( window[0], min_pos) + 1);
					if( window[0] < min_pos || window[1] == x.length || window[1] <= (max_pos -  window[0] + 1)){
						running_sum +=window[1] ;
						if(DEBUG_WINDOW_COMPUTATION){
							System.out.format("Window[pos:%d,size:%d]\n", window[0] , window[1] );
							windows_final.add(window);
						}
					}
					else{
						newWindows.add(window);
					}
				}
				windows_current = newWindows;
			}
			//add window, and update previous
			windows_current.add(new int[]{pos, w});
			prev_min = min_pos;
			//MAX_SUM:
			if(ENABLE_LAZY_WINDOWS && running_sum + (frequency - idx -1 + windows_current.size()) * x.length  > maxMinWindows){
				if(LRU_CACHE)
					cache.put(key, Double.MAX_VALUE);
				return Double.MAX_VALUE;
			}
		}	
		if(DEBUG_WINDOW_COMPUTATION){
			for(int window[]: windows_current){
				System.out.format("Window[pos:%d,size:%d]\n", window[0], window[1]);
			}
			windows_final.addAll(windows_current);
		}
		for(int[] window: windows_current){
			running_sum += window[1];
		}
		if(LRU_CACHE)
			cache.put(key, running_sum);
		return running_sum;
	}
	
	private String toReadablePattern(int[] x) {
		StringBuffer patternReabable = new StringBuffer();
		for(int i=0; i<x.length; i++){
			patternReabable.append(this.frequentItemsLabels[x[i]]).append(" ");
		}
		return patternReabable.toString();
	}
	
	//for testing purposes, only if DEBUG_WINDOW_COMPUTATION
	public List<int[]> getMinimalWindowsLastRun(){
		return this.windows_final;
	}
	
	public File getOutput(){
		return this.output;
	}
	
	private void debugLoadData(int size, int originalSize) {
		if(!DEBUG_LOAD_DATA){
			return;
		}
		Runtime runtime = Runtime.getRuntime();
		System.out.println("Used Memory index:" + (runtime.totalMemory() - runtime.freeMemory()) / (1024*1024) + " Mb");
		System.out.println("Sequence size:" + size);
		System.out.println("All items size:" + originalSize);
		System.out.println("Frequent items size:" + this.frequentItems.length);
		System.out.println("Top 10:");
		for(int i=this.frequentItems.length-1; i> this.frequentItems.length-11 && i>-1; i--)
		{
			System.out.println("   " + this.frequentItemsLabels[i] + ":" + this.frequentItems[i]);
		}
		if(this.frequentItems.length > 10)
		{
			System.out.println("...");
			for(int i=10; i>-1; i--){
				System.out.println("   " + this.frequentItemsLabels[i] + ":" + this.frequentItems[i]);
			}
		}
		if(DEBUG_DICT)
		{
			System.out.println("ALL");
			for(int i=this.frequentItems.length-1; i>-1; i--){
				//System.out.println("   " + this.frequentItemsLabels[i] + ":" + this.frequentItems[i]);
				if((i % (this.frequentItemsLabels.length/200)) == 0){
					System.out.println("=== sup ===   " + this.frequentItems[i] + " " + (this.frequentItems.length - i) + " items");
				}
			}
		}
	}
	
	//for testing:
	//expected string with whitespace, e.g. "a b c", this is transformed to internal (integer) representation
	int[] toPattern(String s){
		String[] tokens = s.split(" ");
		int[] pattern = new int[tokens.length];
		for(int i=0;i<tokens.length;i++){
			String token = tokens[i];
			Integer value = this.tokenToItemIdx.get(token);
			if(value == null){
				throw new RuntimeException(String.format("%s not found", token));
			}
			pattern[i] = value;
		}
		return pattern;
	}
	
	private void printDebug(ArrayDeque<Pair<int[], Integer>> stack,
			long noPatterns, long visits, long startTime, long elapsed) {
		if(!DEBUG_DFS)
			return;
		Runtime rt = Runtime.getRuntime();
		System.out.println("-------------------------------");
		System.out.print("Memory:" + (rt.totalMemory() - rt.freeMemory()) / (1024*1024) + " Mb ");
		System.out.print(" Elapsed: " + Utils.milisToStringReadable(elapsed));
		System.out.print(" Running for: " +  Utils.milisToStringReadable(System.currentTimeMillis() - startTime));
		System.out.print(" #Visits: " + visits);
		System.out.print(" #Patterns: " +  noPatterns);
		System.out.print(" Size stack: " +  stack.size());
		System.out.print("\nTop stack: ");
		int i=0;
		for(Pair<int[],Integer> pair: stack){
			if(i++>2){
				break;
			}
			System.out.print(toReadablePattern(pair.getFirst()));
			System.out.print(", ");
		}
		System.out.println();
		int[] x = stack.peek().getFirst();
		if(x.length > 0){
			System.out.println("Current element: " + x[0] + " of " + (this.frequentItems.length -1) + " (value=" + this.frequentItemsLabels[x[0]] + ")");
		}
	}

	

}
