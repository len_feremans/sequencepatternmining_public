package be.uantwerpen.pattern_mining;

/**
 * Need large array, where each element is a fixed, but variable sized, array.
 * 
 * Why?
 * - We don't want Integer objects (extra pointer), puts int
 * - We don't want millions of empty elements and pointers, 
 *   as induced by  a large number of HashMap<Integer,ArrayList<Integer>> structure.
 * - We wan't to 'push' an element to the free position in our of our fixed list.
 * 
 * See test for performance, but seems thatL
 * - +- 5 times less memory consumption
 * - Algorithm runs much also faster!
 * 
 * @author lfereman
 *
 */
public class IndexStructure {

	private int[][] structure;
	private int[] free; //contains position of first element 
	
	public IndexStructure(int sizes[]){
		this.structure = new int[sizes.length][];
		this.free = new int[sizes.length];
		for(int i=0; i<sizes.length; i++){
			this.structure[i] = new int[sizes[i]];
			this.free[i] = 0;
		}
	}
	
	public void push(int key, int value){
		int value_pos = this.free[key];
		this.structure[key][value_pos] = value;
		this.free[key] = value_pos+1;
	}
	
	public int[] get(int key){
		return this.structure[key];
	}
	
	
}
