package be.uantwerpen.pattern_mining;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import be.uantwerpen.util.CommandLineUtils;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

/**
 * Wrapper for closepi.
 * 
 * Since closepi mines episodes, here all non-parallel episodes are filtered from output.
 * and closure is off, since this would prune parallel episodes in output
 * 
 * @author lfereman
 *
 */
public class WrappperClosedEpisodes {

	public static String BINARY_LOCATION_CLOSEPI = "./other_tools/closedepisodeminer/closepi";
	
	public static File runMethod(File sequence, File labels, double minSupport, int windowSize, 
			boolean useMinimalWindows, boolean useWeightedWindows) throws Exception{
		Timer timer = new Timer("closepi");
		File output = new File("./temp/closepi_output.txt");
		String[] command = new String[]{BINARY_LOCATION_CLOSEPI,
				"-i",sequence.getAbsolutePath(), "-l", labels.getAbsolutePath(),
				"-o",output.getAbsolutePath(),
				"-t",String.valueOf(minSupport),
				"-w",String.valueOf(windowSize),
				"-u",
				"-N", //don't close nodes
				"-E"  //don't close edges
		};
		if(useMinimalWindows){
			command = Utils.concatenate(command,new String[]{"-x"});
		}
		if(useWeightedWindows){
			command = Utils.concatenate(command,new String[]{"-y"});
		}
		int result = CommandLineUtils.runCommand(command);
		if(result!=0){
			throw new RuntimeException("Error running closepi: " + Utils.join(command,","));
 		}
		List<Map<String,String>> patterns = postProcess(output, useMinimalWindows, useWeightedWindows);
		timer.end();
		//save
		String modus = (!useMinimalWindows&&!useWeightedWindows)?"regular":(useMinimalWindows?"min_windows":"weighted");
		File output2 = new File(String.format("./temp/closepi_%s_minSup=%03.3f_window=%03d_modus=%s.csv", 
				Utils.getFilenameNoExtension(sequence), minSupport, windowSize, modus));
		System.out.println("started " + output2.getName());
		FileWriter writer = new FileWriter(output2);
		for(Map<String,String> pattern: patterns){
			writer.write(String.format("%s;%s;%.3f;%d\n", 
					pattern.get("labels"), 
					Integer.valueOf(pattern.get("m-support")),
					Double.valueOf(pattern.get("m-minwin")),  
					pattern.get("labels").split(" ").length));
		}
		writer.close();
		System.out.println("Saved " + output2);
		return output2;
	}
	
	/**
	 * Output like:
graph: 0
size: 1 0
nodes: 0
edges:
type: parallel
labels: call
m-support: 2748
m-minwin: 189.000000
m-fclosed: 0

graph: 1
...
	 * @param outputFile
	 * @throws IOException 
	 */
	public static List<Map<String,String>> postProcess(File outputFile, boolean useMinimalWindows, boolean useWeightedWindows) throws IOException{
		FileStream fs = new FileStream(outputFile, '\n');
		String line = fs.nextToken();
		List<Map<String,String>> patterns = new ArrayList<>();
		int totalEpisodes = 0;
		while(line != null){
			if(line.startsWith("graph:")){
				totalEpisodes++;
				Map<String,String> attributes = new HashMap<String,String>();
				while(true){
					String[] pair = line.split(":\\s+");
					if(pair.length == 2)
						attributes.put(pair[0], pair[1]);
					if(pair[0].equals("m-fclosed"))
						break;
					line = fs.nextToken();
				}
				if(attributes.get("type").equals("parallel") && !attributes.get("size").startsWith("1")){
					patterns.add(attributes);
				}
			}
			line = fs.nextToken();
		}
		if(true)
		Collections.sort(patterns, new Comparator<Map<String,String>>() {

			@Override
			public int compare(Map<String, String> o1, Map<String, String> o2) {
				if(!useMinimalWindows && !useWeightedWindows){
					int sup1 = Integer.valueOf(o1.get("m-support"));
					int sup2 = Integer.valueOf(o2.get("m-support"));
					int len1 = o1.get("labels").split(" ").length;
					int len2 = o2.get("labels").split(" ").length;
					//sort on support, of length if same support
					if(sup1 != sup2){
						return sup2-sup1;
					}
					//else: sort on desc by pattern.length
					return len2-len1;
				}
				else { //else sort on m-minwin
					Double minWin1 = Double.valueOf(o1.get("m-minwin"));
					Double minWin2 = Double.valueOf(o2.get("m-minwin"));
					int len1 = o1.get("labels").split(" ").length;
					int len2 = o2.get("labels").split(" ").length;
					//sort on support, of length if same support
					if(!Utils.equals(minWin1, minWin2)){
						return minWin2.compareTo(minWin1);
					}
					//else: sort on desc by pattern.length
					return len2-len1;
				}
			}
		});
		//print
		//for(Map<String,String> pattern: patterns){
		//	System.out.format("%-20s. Support:%-5d, MinWidth: %-5.1f, Len: %-2d\n", pattern.get("labels"), Integer.valueOf(pattern.get("m-support")),  Double.valueOf(pattern.get("m-minwin")), pattern.get("labels").split(" ").length);
		//}
		System.out.format("Found %d episodes, and %d parallel (size >= 2) episodes.\n", totalEpisodes, patterns.size());
		return patterns;
	}
}
