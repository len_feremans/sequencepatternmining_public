package be.uantwerpen.pattern_mining;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import be.uantwerpen.util.DoubleMap;
import be.uantwerpen.util.FileStream;

/**
 * Closepi required files to be in 'dat' / 'lab' format, 
 * where the ".lab" is  a file with all labels, seperate by a newline
 * e.g.
 * this
 * are
 * the 
 * labels
 * and the '.dat' file is a space seperate file, with all tokens in stream, index with each line in
 * 'lab' file, e.g.
 * 0  s
 * 1   
 * 2
 * 3
 * 1
 * 
 * @author lfereman
 *
 */
public class DatLabFileToNormalFile {

	/**
	 * e.g.moby_whole.dat, moby.lab
	 */
	public static void convert(File dataSequenceFile, File dataDictionaryFile, File outputFile) throws IOException{
		//parse moby.lab
		FileStream fs = new FileStream(dataDictionaryFile, '\n');
		String token = fs.nextToken();
		int pos=0;
		HashMap<Integer,String> indexToToken = new HashMap<>();
		while(token != null){
			indexToToken.put(pos,token);
			token = fs.nextToken();
			pos++;
		}
		//parse moby_whole, and save
		fs = new FileStream(dataSequenceFile, ' ');
		token = fs.nextToken();
		pos=0;
		FileWriter writer = new FileWriter(outputFile);
		while(token != null){
			String tokenAsStr = indexToToken.get(Integer.valueOf(token));
			writer.write(tokenAsStr);
			writer.write(" ");
			token = fs.nextToken();
			if(pos % 10 ==0)
				writer.write("\n"); //newline every 10 tokens for readability
			pos++;
		}
		writer.close();
		System.out.println("Dict size:" + indexToToken.size() + " Sequence size:" + pos );
	}
	
	/**
	 * e.g.moby_whole.dat, moby.lab
	 */
	public static void convertToDataLab(File input, File outputDataSequenceFile, File outputDataDictionaryFile) throws IOException{
		DoubleMap<String,Integer> dict = new DoubleMap<String,Integer>(); //abstraction over TreeMap
		FileStream fs = new FileStream(input,' ','\n');
		String token = fs.nextToken();
		int index = 0;
		while(token !=null){
			if(token.isEmpty()){
				token = fs.nextToken();
				continue;
			}
			if(dict.get(token)==null){
				dict.put(token, index++);
			}
			token = fs.nextToken();
		}
		List<Integer> sequence = new ArrayList<Integer>();
		fs = new FileStream(input,' ','\n');
		token = fs.nextToken();
		while(token !=null){
			if(token.isEmpty()){
				token = fs.nextToken();
				continue;
			}
			int idx = dict.get(token);
			sequence.add(idx);
			token = fs.nextToken();
		}
		//save files
		//outputDataDictionaryFile, ends with '.lab'
		//format token1\ntoken2...
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputDataDictionaryFile));
		for(int i=0; i<dict.keySet().size(); i++){
			writer.write(dict.getByValue(i));
			writer.write("\n");
		}
		writer.close();
		System.out.println("Saved " + outputDataDictionaryFile);
		writer = new BufferedWriter(new FileWriter(outputDataSequenceFile));
		for(Integer i: sequence){
			writer.write(String.valueOf(i));
			writer.write(" ");
		}
		writer.close();
		System.out.println("Saved " + outputDataSequenceFile);
	}
	
}
