package be.uantwerpen.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.lang.reflect.Array;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

/**
 * Generic Utils for running code
 * @author lfereman
 *
 */
@SuppressWarnings("unchecked")
public class Utils {
	
	/**
	 * see http://stackoverflow.com/questions/8519669/replace-non-ascii-character-from-string
	 * Does: 
	 * 		Replace "öäü with oau
	 * 		Remove non-ascii characters
	 * @param name
	 * @return
	 */	
	public static String escapeNonAscii(String name){
		String escapedName = Normalizer.normalize(name, Normalizer.Form.NFD);
		escapedName= escapedName.replaceAll("[^\\x00-\\x7F]", " "); 
		return escapedName.trim();
	}
	
	public static String milisToStringReadable(long milis){
		if(milis < 1000){
			return String.format("%d ms", milis);
		}
		else if(milis >= 1000 && milis < 60000){
			return String.format("%.1f sec", milis/1000.0);
		}
		else if(milis >= 60 * 1000 && milis < 60 * 60 * 1000){
			return String.format("%.1f min", milis/(60 * 1000.0));
		}
		//if(milis >= 360000){
		return String.format("%.2f h", milis/(3600 * 1000.0));		
	}
	
	public static boolean equals(Double x1, Double x2) {
		return Math.abs(x1 -x2) < 0.000001;
	}

	public static void printHead(File file) throws IOException{	
		System.out.println("====== HEAD " + file.getName() + " ========");
		List<String> lines = readFileUntil(file, 10);
		for(String line: lines)
			System.out.println(line);
	}


	
	public static List<String> readFileUntil(File file, int lineNumber) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		String current = reader.readLine();
		int line = 1;
		while(current != null)
		{
			lines.add(current);
			current = reader.readLine(); 
			line++;
			if(line >= lineNumber)
				break;
		}
		reader.close();
		return lines;
	}	

	public static String getFilenameNoExtension(File file) 
	{
		int idx = file.getName().lastIndexOf(".");
		if(idx == -1)
			return file.getName();
		else 
			return file.getName().substring(0, idx);
	}
	

	public static File getFileWithDifferentExtension(File file, String ext) 
	{
		return new File(file.getParentFile(), getFilenameNoExtension(file) + "." + ext);
	}
	

	public static long countLines(File input) throws IOException {
		//see http://stackoverflow.com/questions/453018/number-of-lines-in-a-file-in-java
        LineNumberReader  lnr = new LineNumberReader(new FileReader(input));
        lnr.skip(Long.MAX_VALUE);
        long lines = lnr.getLineNumber();
        // Finally, the LineNumberReader object should be closed to prevent resource leak
        lnr.close();
        return lines;
	}
	
	public static <T> T[] concatenate (T[] A, T[] B) {
		int aLen = A.length;
		int bLen = B.length;
	
		T[] result = (T[]) Array.newInstance(A.getClass().getComponentType(), aLen+bLen);
		for(int i=0; i<aLen; i++)
			result[i] = A[i];
		for(int j=0; j<bLen; j++)
			result[j+ aLen] = B[j];
		return result;
	}
	
	
	public static <T> T[] concatenate (T[]... arrays) {
		int len = 0;
		for(T[] arr: arrays){
			len += arr.length;
		}
		T[] result = (T[]) Array.newInstance(arrays[0].getClass().getComponentType(), len);
		int i=0;
		for(T[] arr: arrays){
			for(T el: arr){
				result[i++] = el;
			}
		}
		return result;
	}


	public static <T> String join(T[] array, String seperator) {
		StringBuffer buffer = new StringBuffer();
		int idx =0;
		for(T el: array)
		{
			buffer.append(el.toString());
			if(idx != array.length -1)
				buffer.append(seperator);
			idx++;
		}
		return buffer.toString();
	}
}
