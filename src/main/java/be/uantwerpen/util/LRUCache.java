package be.uantwerpen.util;

import java.util.LinkedHashMap;
import java.util.Map;

//from http://chriswu.me/blog/a-lru-cache-in-10-lines-of-java/
public class LRUCache<K, V> extends LinkedHashMap<K, V> {
	private int cacheSize;

	public LRUCache(int cacheSize) {
		super(10000, 0.75f, true);
		this.cacheSize = cacheSize;
	}

	protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
		return size() >= cacheSize;
	}
}

