package be.uantwerpen.pattern_mining_experiments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.pattern_mining.DatLabFileToNormalFile;
import be.uantwerpen.pattern_mining.MineCoOccuringItems;
import be.uantwerpen.pattern_mining.PreProcessTextUtils;
import be.uantwerpen.pattern_mining.WrappperClosedEpisodes;
import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Utils;

public class TestExperimentOriginOfSpecies {

	//copy pasted from gutenberg all chapters (no legal header/footer stuff)
	File inputRaw = new File("./data/pg2009.txt"); 
	
	//tokenized/stemmed/stopwords removed/lower case/no special chars:
	File inputProcessed = new File("./data/origin_of_species_processed.txt");

	//in lab/dat format
	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");

	@Test
	public void preProcessMobyDickGutenberg() throws Exception{
		boolean stem = true;
		FileStream fs = new FileStream(inputRaw, '\n');
		String line = fs.nextToken();
		FileWriter writer = new FileWriter(inputProcessed);
		while(line != null){
			line = PreProcessTextUtils.convertLine(line, stem);
			if(!line.isEmpty()){
				writer.write(line);
				writer.write("\n");
			}
			line = fs.nextToken();
		}
		writer.close();
		//make lab files
		DatLabFileToNormalFile.convertToDataLab(inputProcessed,inputProcessed_as_dat, inputProcessed_as_lab);
		DatLabFileToNormalFile.convert(inputProcessed_as_dat, inputProcessed_as_lab, new File("./temp/test_origin_convertion.txt"));
		//sanity check:
		Utils.printHead(inputRaw);
		Utils.printHead(inputProcessed);
		Utils.printHead(inputProcessed_as_dat);
		Utils.printHead(inputProcessed_as_lab);
		Utils.printHead(new File("./temp/test_origin_convertion.txt"));
	}
	
	@Test
	public void runFCI_Seq() throws IOException{
		double minCohesion = 0.01;
		int support = 5; //was 6, changed to 5
		int maxlen = 5;
		MineCoOccuringItems service = new MineCoOccuringItems(inputProcessed_as_lab, inputProcessed_as_dat, support);
		service.minePatterns(maxlen, minCohesion);
		Utils.printHead(service.getOutput());
	}
	
	@Test
	public void runClosepi_sanity() throws Exception{
		double minSupport = 30;
		int windowSize = 15;
		boolean minWindow = false;
		boolean weighted = false;
		File output = WrappperClosedEpisodes.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted);
		Utils.printHead(output);
		//Marbles
		weighted = true;
		minSupport = 1;
		output = WrappperClosedEpisodes.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted);
		Utils.printHead(output);
		//LAXMAN
		weighted = false;
		minWindow = true;
		minSupport = 5;
		output = WrappperClosedEpisodes.runMethod(inputProcessed_as_dat,inputProcessed_as_lab, minSupport, windowSize, minWindow, weighted);
		Utils.printHead(output);
	}
	
	
	
	
}
