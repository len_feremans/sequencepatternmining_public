package be.uantwerpen.pattern_mining_experiments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.pattern_mining.MineCoOccuringItems;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

public class TestExperimentPerformanceOriginMaxLen {

	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
	
	
	@Test
	public void runFCI_Seq_Check_Varying_MaxLen_Performance() throws IOException{
		int support = 250; //was 350
		double minCohesion = 0.015; //
		FileWriter writer = new FileWriter("performance_maxlen_origin.csv");
		int[] maxlens = new int[]{2,4,8,16,32,Integer.MAX_VALUE}; 
		for(int maxlen : maxlens){ 
			MineCoOccuringItems service = new MineCoOccuringItems(inputProcessed_as_lab, inputProcessed_as_dat, support);
			Timer timer = new Timer("maxlen: " + maxlen);
			service.minePatterns(maxlen, minCohesion);
			long elapsed = timer.end();
			long patternsFound = Utils.countLines(service.getOutput());
			System.out.println("Patterns Found: " + patternsFound);
			writer.write(String.format("%s;%d;%d;%.3f;%d;%d\n", 
					"origin", 
					support, maxlen, minCohesion, 
					elapsed, patternsFound));
			writer.flush();
		}
		writer.close();
	}
	
}
