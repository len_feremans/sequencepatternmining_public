package be.uantwerpen.pattern_mining_experiments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.pattern_mining.MineCoOccuringItems;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

public class TestExperimentPerformanceMobyDickCohesion {

	File inputProcessed_as_dat= new File("./data/moby_dick_gutenberg_processed.dat");
	File inputProcessed_as_lab= new File("./data/moby_dick_gutenberg_processed.lab");
	
	@Test
	public void runFCI_Seq_Check_Varying_Cohesion_Performance() throws IOException{
		int maxlen = 5; 
		int support = 4;
		double[] cohesions = new double[]{1/10.0,1/20.0,1/30.0,1/40.0,1/50.0,1/60.0,1/70.0,1/80.0,1/90.0,1/100.0};
		FileWriter writer = new FileWriter("performance_cohesion_moby.csv");
		for(double minCohesion: cohesions){
			MineCoOccuringItems service = new MineCoOccuringItems(inputProcessed_as_lab, inputProcessed_as_dat, support);
			Timer timer = new Timer("cohesion: " + minCohesion);
			service.minePatterns(maxlen, minCohesion);
			long elapsed = timer.end();
			long patternsFound = Utils.countLines(service.getOutput());
			System.out.println("Patterns Found: " + patternsFound);
			writer.write(String.format("%s;%d;%d;%.3f;%d;%d\n", 
					"moby", 
					support, maxlen, minCohesion, 
					elapsed, patternsFound));
			writer.flush();
		}
		writer.close();
	}
	

}
