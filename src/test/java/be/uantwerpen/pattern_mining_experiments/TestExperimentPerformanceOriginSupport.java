package be.uantwerpen.pattern_mining_experiments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.pattern_mining.MineCoOccuringItems;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

public class TestExperimentPerformanceOriginSupport {


	File inputProcessed_as_dat= new File("./data/origin_of_species_processed.dat");
	File inputProcessed_as_lab= new File("./data/origin_of_species_processed.lab");
	
	@Test
	public void runFCI_Seq_Check_Varying_Support_Performance() throws IOException{
		int maxlen = 4; //was 6
		double minCohesion = 0.010; //was 0.01
		MineCoOccuringItems.DEBUG_DICT = true;
		MineCoOccuringItems service = new MineCoOccuringItems(inputProcessed_as_lab, inputProcessed_as_dat, 2);
		MineCoOccuringItems.DEBUG_DICT = false;
		int supports[] = new int[]{470,140,36,16,10,5,3,2};
		FileWriter writer = new FileWriter("performance_support_origin.csv");
		for(int support: supports){
			service = new MineCoOccuringItems(inputProcessed_as_lab, inputProcessed_as_dat, support);
			Timer timer = new Timer("support: " + support);
			service.minePatterns(maxlen, minCohesion);
			long elapsed = timer.end();
			long patternsFound = Utils.countLines(service.getOutput());
			System.out.println("Patterns Found: " + patternsFound);
			writer.write(String.format("%s;%d;%d;%.3f;%d;%d\n", 
					"origin", 
					support, maxlen, minCohesion, 
					elapsed, patternsFound));
			writer.flush();
		}
		writer.close();
	}
	

}
