package be.uantwerpen.pattern_mining_experiments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Test;

import be.uantwerpen.pattern_mining.MineCoOccuringItems;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

public class TestExperimentPerformanceMobyDickSupport {

	File inputProcessed_as_dat= new File("./data/moby_dick_gutenberg_processed.dat");
	File inputProcessed_as_lab= new File("./data/moby_dick_gutenberg_processed.lab");
	
	@Test
	public void runFCI_Seq_Check_Varying_Support_Performance() throws IOException{
		int maxlen = 4; //was 10
		double minCohesion = 0.010; //was 0.01
		int[] supports = new int[]{360, 100, 10, 5, 3, 2};
		FileWriter writer = new FileWriter("performance_support_moby.csv");
		MineCoOccuringItems.DEBUG_DICT = true;
		MineCoOccuringItems service = new MineCoOccuringItems(inputProcessed_as_lab, inputProcessed_as_dat, 2);
		MineCoOccuringItems.DEBUG_DICT = false;
		for(int support : supports){
			service = new MineCoOccuringItems(inputProcessed_as_lab, inputProcessed_as_dat, support);
			Timer timer = new Timer("support speed: " + support);
			service.minePatterns(maxlen, minCohesion);
			long elapsed = timer.end();
			long patternsFound = Utils.countLines(service.getOutput());
			System.out.println("Patterns Found: " + patternsFound);
			writer.write(String.format("%s;%d;%d;%.3f;%d;%d\n", 
					"moby", 
					support, maxlen, minCohesion, 
					elapsed, patternsFound));
			writer.flush();
		}
		writer.close();
	}
	

}
