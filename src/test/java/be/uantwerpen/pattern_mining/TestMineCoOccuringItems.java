package be.uantwerpen.pattern_mining;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import be.uantwerpen.util.FileStream;
import be.uantwerpen.util.Pair;
import be.uantwerpen.util.Timer;
import be.uantwerpen.util.Utils;

/**
 * Testing computation in MineCoOccuringItems
 * 
 * Note: Was really really necessary
 * @author lfereman
 *
 */
public class TestMineCoOccuringItems {
	
	private File[] makeTestSequence(String sequence) throws IOException{
		char[] chars = sequence.toCharArray();
		List<Character> unique = new ArrayList<Character>();
		for(char ch: chars){
			boolean found = unique.contains(new Character(ch));
			if(!found){
				unique.add(ch);
			}
		}
		List<Integer> sequenceLst = new ArrayList<Integer>();
		for(char ch: chars){
			sequenceLst.add(unique.indexOf(new Character(ch)));
		}
		//save:
		FileWriter fw = new FileWriter("./temp/" + sequence.substring(0, 10) + ".lab");
		for(Character ch: unique){
			fw.write(ch);
			fw.write("\n");
		}
		fw.close();
		fw = new FileWriter("./temp/" + sequence.substring(0, 10)  + ".dat");
		for(Integer i: sequenceLst){
			fw.write("" + i);
			fw.write(" ");
		}
		fw.close();
		return new File[]{new File("./temp/" + sequence.substring(0, 10)  + ".lab"), new File("./temp/" + sequence.substring(0, 10)  + ".dat")};
	}
	
	@Test
	public void testWindowWidth1() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "abceeeabc";
		File[] files = makeTestSequence(data);
		String pattern = "a b";
		System.out.println("data:" + data + " pattern:" + pattern);
		MineCoOccuringItems service = new MineCoOccuringItems(files[0], files[1], 1);
		MineCoOccuringItems.DEBUG_WINDOW_COMPUTATION = true;
		double w= service.sumMinimalWindows(service.toPattern(pattern));
		System.out.println(w);
		List<int[]> windows = service.getMinimalWindowsLastRun();
		assertEquals(w,8.0,0.001);
		assertTrue(Arrays.equals(windows.get(0),new int[]{0,2}));
		assertTrue(Arrays.equals(windows.get(1),new int[]{1,2}));
		assertTrue(Arrays.equals(windows.get(2),new int[]{6,2}));
		assertTrue(Arrays.equals(windows.get(3),new int[]{7,2}));
	}
	
	
	@Test
	public void testWindowWidth2() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "cghedabacadejckdlcmd";
		File[] files = makeTestSequence(data);
		String pattern = "a c d";
		System.out.println("data:" + data + " pattern:" + pattern);
		MineCoOccuringItems service = new MineCoOccuringItems(files[0],files[1], 2);
		MineCoOccuringItems.DEBUG_WINDOW_COMPUTATION = true;
		double w= service.sumMinimalWindows(service.toPattern(pattern));
		List<int[]> windows = service.getMinimalWindowsLastRun();
		System.out.println(w);
		assertEquals(w,61,0.001);
		assertTrue(Arrays.equals(windows.get(0),new int[]{0,6}));
		assertTrue(Arrays.equals(windows.get(6),new int[]{10,3}));
		assertTrue(Arrays.equals(windows.get(10),new int[]{19,11}));
	}
	
	@Test
	public void testWindowWidth3() throws IOException{
		System.out.println("======= TEST WINDOW COMPUTATION =======");
		String data = "abceeeabc";
		File[] files = makeTestSequence(data);
		String pattern = "e c";
		System.out.println("data:" + data + " pattern:" + pattern);
		MineCoOccuringItems service = new MineCoOccuringItems(files[0], files[1], 1);
		MineCoOccuringItems.DEBUG_WINDOW_COMPUTATION = true;
		double w= service.sumMinimalWindows(service.toPattern(pattern));
		System.out.println(w);
		assertEquals(w,15.0,0.001);
	}
	
	@Test
	public void testCohesion1() throws IOException{
		System.out.println("======= TEST COHESION COMPUTATION =======");
		String data = "abceeeabc";
		File[] files = makeTestSequence(data);
		System.out.println("data:" + data);
		MineCoOccuringItems service = new MineCoOccuringItems(files[0],files[1], 1);
		MineCoOccuringItems.DEBUG_WINDOW_COMPUTATION = true;
		for(String pattern: new String[]{"a","a b","a b c","e c"}){
			int[] p = service.toPattern(pattern);
			double cohesion = service.cohesion(p);
			System.out.format("pattern %s, cohesion: %f\n", pattern, cohesion);
			assertEquals(pattern.contains("a")?1.0:0.666, cohesion, 0.01);
		}
	}
	
	@Test
	public void testMine1() throws IOException{
		System.out.println("======= TEST DFS COMPUTATION =======");
		Timer.VERBOSE = false;
		String data = "abceeeabc";
		File[] files = makeTestSequence(data);
		System.out.println("data:" + data);
		MineCoOccuringItems service = new MineCoOccuringItems(files[0],files[1], 1);
		MineCoOccuringItems.DEBUG_DFS = true;
		MineCoOccuringItems.DEBUG_WINDOW_COMPUTATION = true;
		service.minePatterns(1000,0.001);
		Utils.printHead(service.getOutput());
	}
	
	@Test
	public void testMine2() throws IOException{
		System.out.println("======= TEST DFS COMPUTATION =======");
		Timer.VERBOSE = false;
		String data = "acefbaacdeafdg";
		File[] files = makeTestSequence(data);
		System.out.println("data:" + data);
		MineCoOccuringItems service = new MineCoOccuringItems(files[0],files[1], 1);
		MineCoOccuringItems.DEBUG_DFS = false;
		MineCoOccuringItems.DEBUG_WINDOW_COMPUTATION = true;
		service.minePatterns( 10000, 0.001);
		Utils.printHead(service.getOutput());
	}
	
	@Test
	public void fragmentMobyTest() throws IOException{
		String data = "All ye mast-headers have before now heard me give orders about a white whale. Look ye! d'ye see this Spanish ounce of gold?\"—holding up a broad bright coin to the sun—\"it is a sixteen dollar piece, men. D'ye see it? Mr. Starbuck, hand me yon top-maul.\" While the mate was getting the hammer, Ahab, without speaking, was slowly rubbing the gold piece against the skirts of his jacket, as if to heighten its lustre, and without using any words was meanwhile lowly humming to himself, producing a sound so strangely muffled and inarticulate that it seemed the mechanical humming of the wheels of his vitality in him. Receiving the top-maul from Starbuck, he advanced towards the main-mast with the hammer uplifted in one hand, exhibiting the gold with the other, and with a high raised voice exclaiming: \"Whosoever of ye raises me a white-headed whale with a wrinkled brow and a crooked jaw; whosoever of ye raises me that white-headed whale, with three holes punctured in his starboard fluke—look ye, whosoever of ye raises me that same white whale, he shall have this gold ounce, my boys!\" \"Huzza! huzza!\" cried the seamen, as with swinging tarpaulins they hailed the act of nailing the gold to the mast. \"It's a white whale, I say,\" resumed Ahab, as he threw down the topmaul: \"a white whale. Skin your eyes for him, men; look sharp for white water; if ye see but a bubble, sing out.\" All this while Tashtego, Daggoo, and Queequeg had looked on with even more intense interest and surprise than the rest, and at the mention of the wrinkled brow and crooked jaw they had started as if each was separately touched by some specific recollection. \"Captain Ahab,\" said Tashtego, \"that white whale must be the same that some call Moby Dick.\" \"Moby \"Dick\"Dick?\" shouted Ahab. \"Do ye know the white whale then, Tash?\" \"Does he fan-tail a little curious, sir, before he goes down?\" said the Gay-Header deliberately. \"And has he a curious spout, too,\" said Daggoo, \"very bushy, even for a parmacetty, and mighty quick, Captain Ahab?\" \"And he have one, two, three—oh! good many iron in him hide, too, Captain,\" cried Queequeg disjointedly, \"all twiske-tee be-twisk, like him—him—\" faltering hard for a word, and screwing his hand round and round as though uncorking a bottle—\"like him—him—\" \"Corkscrew!\" cried Ahab, \"aye, Queequeg, the harpoons lie all twisted and wrenched in him; aye, Daggoo, his spout is a big one, like a whole shock of wheat, and white as a pile of our Nantucket wool after the great annual sheep-shearing; aye, Tashtego, and he fan-tails like a split jib in a squall. Death and devils! men, it is Moby Dick ye have seen—Moby Dick—Moby Dick!";
		data = data.replaceAll("-", " ");
		data = data.replaceAll("—", " ");
		data = data.replaceAll("\"", " ");
		data = data.replaceAll("\\.", " ");
		data = data.replaceAll("\\?", " ");
		data = data.replaceAll("\\!", " ");
		data = data.replaceAll("\\,", " ");
		data = data.replaceAll("\\;", " ");
		data = data.toLowerCase();
		FileWriter writer = new FileWriter(new File("moby-frag.txt"));
		writer.write(data);
		writer.close();
		DatLabFileToNormalFile.convertToDataLab(new File("moby-frag.txt"),new File("moby-frag.dat"), new File("moby-frag.lab"));
		MineCoOccuringItems service = new MineCoOccuringItems(new File("moby-frag.lab"),new File("moby-frag.dat"), 3);
		double d = service.cohesion(service.toPattern("moby dick"));
		System.out.println("d:" + d);
		double d2 = service.cohesion(service.toPattern("white whale"));
		System.out.println("d:" + d2);
		double d3 = service.cohesion(service.toPattern("captain ahab"));
		System.out.println("d:" + d3);
		//MineCoOccuringItems.DEBUG_DFS = false;
		//MineCoOccuringItems.DEBUG_WINDOW_COMPUTATION = true;
		service.minePatterns( 5, 0.03);
		Utils.printHead(service.getOutput());
		FileStream fs = new FileStream(new File("temp/mine_patterns_moby-frag_minSup=003_minCohesion=0.0300_modus=MEAN_maxLen=005.csv"), '\n');
		String line = fs.nextToken();
		List<Pair<String,Double>> pairs = new ArrayList<>();
		while(line != null){
			if(line.isEmpty())
				continue;
			String[] tokens = line.split(";");
			pairs.add(new Pair<>(tokens[0],Double.valueOf(tokens[3])));
			line = fs.nextToken();
		}
		Collections.sort(pairs, (p1,p2) -> p2.getSecond().compareTo(p1.getSecond()));
		System.out.println("here:");
		for(int i=0;i<50;i++){
			System.out.println(pairs.get(i));
		}
		writer.close();
		
	}
	
}
