Implementation for efficient discovery of sets of co-occuring items in sequences,
by Boris Cule, and Len Feremans.

PAPER:
============================
Efficient Discovery of Sets of Co-occurring Items in Event Sequences
Boris Cule, Len Feremans (University of Antwerp, Belgium)
and Bart Goethals (Monash University, Australia)
Abstract:
Discovering patterns in long event sequences is an important data mining task. 
Most existing work focuses on frequency-based quality measures that allow algorithms 
to use the anti-monotonicity property to prune the search space and efficiently 
discover the most frequent pat- terns. In this work, we step away from such measures, 
and evaluate pat- terns using cohesion — a measure of how close to each other the items 
making up the pattern appear in the sequence on average. We tackle the fact that cohesion 
is not an anti-monotonic measure by developing a novel pruning technique in order to reduce 
the search space. By doing so, we are able to efficiently unearth rare, but strongly cohesive, 
patterns that existing methods often fail to discover.


EXAMPLE:
============================
Given sequence such as abceeeabc, find long-or-short frequent itemssets,
that are cohesive (thus have few 'gaps' on average, or more formally, the smallest
minimal interval). Ranked patterns in abceeeabc, look like:
[a, b, c]           : cohesion: 1.000 = 3 (len) * 6 (freq) / 18(widths)
[a, b]              : cohesion: 1.000 = 2 (len) * 4 (freq) / 8 (widths)
[b, c]              : cohesion: 1.000 = 2 (len) * 4 (freq) / 8 (widths)
[e, a, b, c]        : cohesion: 0.973 = 4 (len) * 9 (freq) / 37(widths)
[e, a, b]           : cohesion: 0.840 = 3 (len) * 7 (freq) / 25(widths)
[e, b, c]           : cohesion: 0.808 = 3 (len) * 7 (freq) / 26(widths)
[e, a, c]           : cohesion: 0.724 = 3 (len) * 7 (freq) / 29(widths)
[e, a]              : cohesion: 0.667 = 2 (len) * 5 (freq) / 15(widths)
[a, c]              : cohesion: 0.667 = 2 (len) * 4 (freq) / 12(widths)
[e, b]              : cohesion: 0.625 = 2 (len) * 5 (freq) / 16(widths)
[e, c]              : cohesion: 0.588 = 2 (len) * 5 (freq) / 17(widths)

USAGE:
============================
Main class:  be.uantwerpen.pattern_mining.MineCoOccuringItems
INPUT FILE is a stream of tokens (coded as string or whatever), seperated by newline or spaces.
  E.g. a raw text file can be given as input, but it's recommended to apply stemming/stopwords
  etc. first.
e.g.
  inputProcessed = new File("./data/moby_dick_gutenberg.txt")
  double minCohesion = 0.01;
  int maxlen = 5;
  int support = 4;
  MineCoOccuringItems service = new MineCoOccuringItems(inputProcessed, support);
  service.minePatterns(maxlen, minCohesion, Modus.MEAN); //saves patterns, unsorted
  
		
DEPENDENCIES / INSTALL
============================
- Java 8, Junit 4 for testing and maven 3
- Snowball-stemmer, and weka, for pre-processing text in experiments (as configured in maven pom.xml)
- For comparision with WINEPI, LAXMAN and MARBLES: closedepisodeminer is downloaded from http://users.ics.aalto.fi/ntatti/software/closedepisodeminer.zip
  You need it compiled and with binary in ./other_tools/closedepisodeminer/closepi,
- For python reporting of experiments python2.7 (or python 3.4) with pandas (0.18), numpy (1.11) and matplotlib (1.1.1) was used.

Compile using:
>>mvn clean install -DskipTests=True

RE-PRODUCE EXPERIMENTS PAPER
============================
- Data is in ./data (downloaden from gutenberg.org, see ./data/licence.txt) 
- Experiments with correct parameters, are coded as Java unit test. Pre-processing is coded in TestExperimentMobyDick,
  and TestExperimentOriginOfSpecies
- Alternative methods, implemented by closed episode miner, are run from Java, see also previous tests
- Python is used for generated top-k sorted results, and overlap, and to make performance plots

Rerun experiments: (see also source scripts)
>>./run_experiments_compare.sh 
>>./run_experiments_performance.sh

LICENCE
=======
Copyright (c) [2016] [Universiteit Antwerpen - Len Feremans]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
