#Performance experiments
nohup mvn clean install -Dtest=be.uantwerpen.pattern_mining_experiments.TestExperimentPerformanceMobyDickCohesion > moby_performance_cohesion.log &
sleep 10
nohup mvn clean install -Dtest=be.uantwerpen.pattern_mining_experiments.TestExperimentPerformanceMobyDickSupport > moby_performance_support.log &
sleep 10
nohup mvn clean install -Dtest=be.uantwerpen.pattern_mining_experiments.TestExperimentPerformanceMobyDickMaxLen > moby_performance_maxlen.log &
sleep 10
nohup mvn clean install -Dtest=be.uantwerpen.pattern_mining_experiments.TestExperimentPerformanceOriginCohesion > origin_performance_cohesion.log &
sleep 10
nohup mvn clean install -Dtest=be.uantwerpen.pattern_mining_experiments.TestExperimentPerformanceOriginSupport > origin_performance_support.log &
sleep 10
nohup mvn clean install -Dtest=be.uantwerpen.pattern_mining_experiments.TestExperimentPerformanceOriginMaxLen > origin_performance_maxlen.log &
sleep 10

#e.g. manually do something like
#tail -n +1 *.csv > output.csv
#cd src/main/python
#python plots_performance.py
