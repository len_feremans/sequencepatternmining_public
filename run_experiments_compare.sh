#e.g. first manually do something like
#cd other_tools
#unzip -d closedepisodeminer closedepisodeminer.zip
#cd closedepisodeminer 
#make

#Comparative experiments
nohup mvn clean install -Dtest=be.uantwerpen.pattern_mining_experiments.TestExperimentMobyDick > moby.log &
sleep 10
nohup mvn clean install -Dtest=be.uantwerpen.pattern_mining_experiments.TestExperimentOriginOfSpecies > species.log &
sleep 10

#e.g. manually do something like
#cd src/main/python
#python make_report_topK_overlap_lengths.py moby
#python make_report_topK_overlap_lengths.py origin


